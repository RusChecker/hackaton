FROM node:alpine
MAINTAINER Barinov Vladimir


RUN apk update
RUN apk add --virtual build-dependencies build-base gcc wget git bash nasm
RUN apk add --no-cache --update git autoconf automake
RUN apk add --no-cache --update libpng-dev
RUN git clone https://github.com/nomad354/hakaton.git
RUN npm rebuild
WORKDIR hakaton
RUN npm install
RUN npm install gulp -g
EXPOSE 3000
EXPOSE 3001
CMD gulp dev