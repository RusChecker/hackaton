# Hakaton

This repo is a gulp starter for project assembly and developer mode. 

Remember to do run the `npm install` command after cloning this repo :)

Project assembly run the `gulp build`

Developer mode run the `gulp dev`